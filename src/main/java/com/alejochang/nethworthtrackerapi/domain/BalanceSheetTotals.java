package com.alejochang.nethworthtrackerapi.domain;

import java.math.BigDecimal;

public class BalanceSheetTotals {
	private BigDecimal assetsTotal;
	private BigDecimal liabilitiesTotal;
	private BigDecimal netWorthTotal;
	
	public BalanceSheetTotals(BigDecimal assetsTotal, BigDecimal liabilitiesTotal, BigDecimal netWorthTotal) {
		super();
		this.assetsTotal = assetsTotal;
		this.liabilitiesTotal = liabilitiesTotal;
		this.netWorthTotal = netWorthTotal;
	}
	
	public BigDecimal getAssetsTotal() {
		return assetsTotal;
	}
	public void setAssetsTotal(BigDecimal assetsTotal) {
		this.assetsTotal = assetsTotal;
	}
	public BigDecimal getLiabilitiesTotal() {
		return liabilitiesTotal;
	}
	public void setLiabilitiesTotal(BigDecimal liabilitiesTotal) {
		this.liabilitiesTotal = liabilitiesTotal;
	}
	public BigDecimal getNetWorthTotal() {
		return netWorthTotal;
	}
	public void setNetWorthTotal(BigDecimal netWorthTotal) {
		this.netWorthTotal = netWorthTotal;
	}
}
