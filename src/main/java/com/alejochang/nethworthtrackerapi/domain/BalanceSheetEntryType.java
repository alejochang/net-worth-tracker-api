package com.alejochang.nethworthtrackerapi.domain;

public enum BalanceSheetEntryType {
	SHORT_TERM,
	LONG_TERM
}
