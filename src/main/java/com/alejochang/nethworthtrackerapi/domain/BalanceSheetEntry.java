package com.alejochang.nethworthtrackerapi.domain;



import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

public class BalanceSheetEntry {
	private String description;
	private BigDecimal amount;
	private Currency currency;
	private BalanceSheetEntryType type;
	
	public BalanceSheetEntry() {
		super();
		this.description = "";
		this.currency = Currency.getInstance(Locale.US);
		this.amount = new BigDecimal(0);
		this.type = BalanceSheetEntryType.SHORT_TERM;
	}
	
	public BalanceSheetEntry(String description, BigDecimal amount, Currency currency, BalanceSheetEntryType type) {
		super();
		this.description = description;
		this.amount = amount;
		this.currency = currency;
		this.type = type;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	public BalanceSheetEntryType getType() {
		return type;
	}

	public void setType(BalanceSheetEntryType type) {
		this.type = type;
	}
}
