package com.alejochang.nethworthtrackerapi.domain;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

public class BalanceSheet {
	
	private Currency currency;
	
	private List<BalanceSheetEntry> assets;
	
	private List<BalanceSheetEntry> liabilities;
	
	public BalanceSheet() {
		super();
		this.currency = Currency.getInstance(Locale.US);
		this.assets = new ArrayList<BalanceSheetEntry>();
		this.liabilities = new ArrayList<BalanceSheetEntry>();
	}
	
	public BalanceSheet(Currency currency, List<BalanceSheetEntry> assets, List<BalanceSheetEntry> liabilities) {
		super();
		this.currency = currency;
		this.assets = assets;
		this.liabilities = liabilities;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Currency getCurrency() {
		return currency;
	}

	public List<BalanceSheetEntry> getAssets() {
		return assets;
	}

	public void setAssets(List<BalanceSheetEntry> assets) {
		this.assets = assets;
	}

	public List<BalanceSheetEntry> getLiabilities() {
		return liabilities;
	}

	public void setLiabilities(List<BalanceSheetEntry> liabilities) {
		this.liabilities = liabilities;
	}

}
