package com.alejochang.nethworthtrackerapi.controllers;

import java.util.Currency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alejochang.nethworthtrackerapi.domain.BalanceSheet;
import com.alejochang.nethworthtrackerapi.domain.BalanceSheetTotals;
import com.alejochang.nethworthtrackerapi.services.INetWorthService;
import com.alejochang.nethworthtrackerapi.utils.IJsonResponseBuilder;


@RestController
@RequestMapping(path = "/balancesheet", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BalanceSheetController {
	
	@Autowired
	private INetWorthService netWorthService;
	
	@Autowired
	private IJsonResponseBuilder jsonResponseBuilder;
	
	@PutMapping(value="/totals", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> calculateNethWorth(@RequestBody BalanceSheet balanceSheet) {
		try {
			BalanceSheetTotals totals = netWorthService.calculateBalanceSheetTotals(balanceSheet);
			
			return ResponseEntity.ok(jsonResponseBuilder.buildJson(totals));
		} catch (Exception e) {
			return 	ResponseEntity.badRequest().build();
		}
	}
	
	
	@PostMapping(value="/convert/{currencyCode}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> convertBalanceSheet(@RequestBody BalanceSheet balanceSheet, @PathVariable String currencyCode) {
		try {
			Currency tradeCurrency = Currency.getInstance(currencyCode);
			
			BalanceSheet convertedBalanceSheet = netWorthService.applyFXToBalanceSheet(balanceSheet, tradeCurrency);
			
			return ResponseEntity.ok(jsonResponseBuilder.buildJson(convertedBalanceSheet));
		
		} catch (Exception e) {
			System.err.println(e);
			return 	ResponseEntity.badRequest().build();
		}
	}

}
