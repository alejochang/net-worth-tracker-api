package com.alejochang.nethworthtrackerapi.services;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alejochang.nethworthtrackerapi.domain.BalanceSheet;
import com.alejochang.nethworthtrackerapi.domain.BalanceSheetEntry;
import com.alejochang.nethworthtrackerapi.domain.BalanceSheetTotals;
import com.alejochang.nethworthtrackerapi.utils.IMoneyCalculator;

@Service
public class NetWorthService implements INetWorthService {

	private final IMoneyCalculator moneyCalculator;

	private final IExchangeService exchangeService;

	@Autowired
	NetWorthService(IMoneyCalculator moneyCalculator, IExchangeService exchangeService) {
		super();
		this.moneyCalculator = moneyCalculator;
		this.exchangeService = exchangeService;
	}

	@Override
	public BalanceSheetTotals calculateBalanceSheetTotals(BalanceSheet balanceSheet) {
		BigDecimal assetsTotal = calculateTotal(balanceSheet.getAssets());

		BigDecimal liabilitiesTotal = calculateTotal(balanceSheet.getLiabilities());

		BigDecimal netWorthTotal = calculateNetWorth(assetsTotal, liabilitiesTotal);

		return new BalanceSheetTotals(assetsTotal, liabilitiesTotal, netWorthTotal);

	}

	private BigDecimal calculateNetWorth(BigDecimal assets, BigDecimal liabilities) {
		return moneyCalculator.executeSubtraction(assets, liabilities);
	}

	private BigDecimal calculateTotal(List<BalanceSheetEntry> entries) {
		if (entries != null && !entries.isEmpty()) {

			Stream<BalanceSheetEntry> entriesStream = entries.stream();
			Stream<BigDecimal> amounts = entriesStream.map(entry -> entry.getAmount());

			return amounts.reduce(BigDecimal.ZERO, moneyCalculator::executeAddition);
		} else {
			return new BigDecimal(0);
		}
	}

	@Override
	public BalanceSheet applyFXToBalanceSheet(BalanceSheet balanceSheet, Currency tradeCurrency) {
		BigDecimal exchangeRate = exchangeService.getExchangeRate(balanceSheet.getCurrency(), tradeCurrency);

		List<BalanceSheetEntry> convertedAssets = balanceSheet.getAssets().stream()
				.map(entry -> this.convertEntry(entry, tradeCurrency, exchangeRate)).collect(Collectors.toList());
		List<BalanceSheetEntry> convertedLiability = balanceSheet.getLiabilities().stream()
				.map(entry -> this.convertEntry(entry, tradeCurrency, exchangeRate)).collect(Collectors.toList());

		return new BalanceSheet(tradeCurrency, convertedAssets, convertedLiability);
	}

	private BalanceSheetEntry convertEntry(BalanceSheetEntry entry, Currency tradeCurrency, BigDecimal exchangeRate) {
		BigDecimal convertedAmount = moneyCalculator.multiplyByRate(entry.getAmount(), exchangeRate);

		return new BalanceSheetEntry(entry.getDescription(), convertedAmount, tradeCurrency, entry.getType());
	}

}
