package com.alejochang.nethworthtrackerapi.services;

import java.math.BigDecimal;
import java.util.Currency;

public interface IExchangeService {
	
	public BigDecimal getExchangeRate(Currency baseCurrency, Currency tradeCurrency);
}
