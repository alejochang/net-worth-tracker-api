package com.alejochang.nethworthtrackerapi.services;

import java.math.BigDecimal;
import java.util.Currency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


@Service
public class ExchangeService implements IExchangeService {
	
//	final String EXCHANGE_URI = "https://api.exchangeratesapi.io/latest";
	final String EXCHANGE_URI = "https://api.exchangerate.host/convert";
	
	final String CRYPTOCURRENCY_EXCHANGE_API_KEY = "9ebcd858-9039-4e5e-8c14-fb475fae0f43";
	final String CRYPTOCURRENCY_EXCHANGE_URI = "9ebcd858-9039-4e5e-8c14-fb475fae0f43";
	
	private final RestTemplate restTemplate;
	
	@Autowired
	ExchangeService(RestTemplateBuilder restTemplateBuilder ){
		super();
		this.restTemplate = restTemplateBuilder.build();
	}

	@Override
	public BigDecimal getExchangeRate(Currency baseCurrency, Currency tradeCurrency) {
			StringBuilder uri = new StringBuilder(EXCHANGE_URI);
			uri.append("?from=");
			uri.append(baseCurrency.getCurrencyCode());
			uri.append("&to=");
			uri.append(tradeCurrency.getCurrencyCode());
			
		    String result = restTemplate.getForObject(uri.toString(), String.class);

		    JsonObject parsedResult = new Gson().fromJson(result, JsonObject.class);
		    
		    try {
		    	return parsedResult.get("info").getAsJsonObject().get("rate").getAsBigDecimal();
			} catch (Exception e) {
				 return BigDecimal.ZERO;
			}
		    
	}
		    

}
