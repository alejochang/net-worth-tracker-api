package com.alejochang.nethworthtrackerapi.services;

import java.util.Currency;

import com.alejochang.nethworthtrackerapi.domain.BalanceSheet;
import com.alejochang.nethworthtrackerapi.domain.BalanceSheetTotals;

public interface INetWorthService {

	BalanceSheetTotals calculateBalanceSheetTotals(BalanceSheet balanceSheet);
	
	BalanceSheet applyFXToBalanceSheet(BalanceSheet balanceSheet, Currency tradeCurrency);

}