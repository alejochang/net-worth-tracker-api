package com.alejochang.nethworthtrackerapi.utils;

public interface IJsonResponseBuilder {
	
	public <T> String buildJson(T entity);

}
