package com.alejochang.nethworthtrackerapi.utils;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class JsonResponseBuilder implements IJsonResponseBuilder{
	
	public <T> String buildJson(T entity) {
		Gson gson = new GsonBuilder().registerTypeAdapter(BigDecimal.class, new BigDecimalJsonSerializer()).create();
		return gson.toJson(entity);
	}
}
