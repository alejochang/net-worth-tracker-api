package com.alejochang.nethworthtrackerapi.utils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;


public class BigDecimalJsonSerializer implements JsonSerializer<BigDecimal> {

	@Override
	public JsonElement serialize(BigDecimal src, Type typeOfSrc, JsonSerializationContext context) {
		return new JsonPrimitive(src.setScale(2, RoundingMode.HALF_EVEN).toString());
	}

}
