package com.alejochang.nethworthtrackerapi.utils;

import java.math.BigDecimal;
import java.util.stream.Stream;

public interface IMoneyCalculator {
	
	public BigDecimal addAmounts(BigDecimal ...amounts);
	
	public BigDecimal addAmounts(Stream<BigDecimal> amounts);
	
	public BigDecimal substractAmounts(BigDecimal ...amounts);
	
	public BigDecimal substractAmounts(Stream<BigDecimal> amounts);
	
	public BigDecimal multiplyByRate(BigDecimal amount, BigDecimal rate);
	
	public BigDecimal executeAddition(BigDecimal augend, BigDecimal addend);
	
	public BigDecimal executeSubtraction(BigDecimal minuend, BigDecimal subtrahend);
	
}
