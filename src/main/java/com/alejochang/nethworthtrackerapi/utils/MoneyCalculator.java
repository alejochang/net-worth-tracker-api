package com.alejochang.nethworthtrackerapi.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

@Component
public class MoneyCalculator implements IMoneyCalculator {
	
	@Override
	public BigDecimal addAmounts(BigDecimal... amounts) {
		Stream<BigDecimal> amountStream = Arrays.stream(amounts);
		return addAmounts(amountStream);
	}
	
	@Override
	public BigDecimal addAmounts(Stream<BigDecimal> amounts) {
		return applyOperationToStream(amounts, this::executeAddition);
	}	
	
	@Override
	public BigDecimal substractAmounts(BigDecimal... amounts) {
		Stream<BigDecimal> amountStream = Arrays.stream(amounts);
		return substractAmounts(amountStream);
	}

	@Override
	public BigDecimal substractAmounts(Stream<BigDecimal> amounts) {
		return applyOperationToStream(amounts, this::executeSubtraction);
	}
	
	private BigDecimal applyOperationToStream(Stream<BigDecimal> amounts, BinaryOperator<BigDecimal> accumulator) {
		return (amounts!=null && amounts.count() > 0) ? amounts.reduce(BigDecimal.ZERO, accumulator) : BigDecimal.ZERO;
	}
	
	@Override
	public BigDecimal executeAddition(BigDecimal augend, BigDecimal addend) {
		return augend.add(addend);
	}
	
	@Override
	public BigDecimal executeSubtraction(BigDecimal minuend, BigDecimal subtrahend) {
		return minuend.subtract(subtrahend);
	}
	
	@Override
	public BigDecimal multiplyByRate(BigDecimal amount, BigDecimal rate) {
		if(amount !=null && rate != null) {
			return amount.multiply(rate);
		}
		return null;
	}

}
