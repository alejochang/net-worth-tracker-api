package com.alejochang.nethworthtrackerapi.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;



@RestClientTest(ExchangeService.class)
public class ExchangeServiceTests {
 
    @Autowired
    private ExchangeService client;
 
    @Autowired
    private MockRestServiceServer server;
 
    
 
    @Test
    public void requestingAFxRateCallsRestApiExchange() 
      throws Exception {
    	StringBuilder uri = new StringBuilder("https://api.exchangerate.host/convert");
		uri.append("?from=USD&to=EUR");
        String rateString = "{info:{rate:1.5}}";
        
        this.server.expect(requestTo(uri.toString()))
          .andRespond(withSuccess(rateString, MediaType.APPLICATION_JSON));
 
        BigDecimal rate = this.client.getExchangeRate(Currency.getInstance("USD"), Currency.getInstance("EUR"));
 
        assertThat(rate).isEqualTo(BigDecimal.valueOf(1.5));
    }
}