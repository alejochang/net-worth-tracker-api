package com.alejochang.nethworthtrackerapi.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.alejochang.nethworthtrackerapi.domain.BalanceSheet;
import com.alejochang.nethworthtrackerapi.domain.BalanceSheetEntry;
import com.alejochang.nethworthtrackerapi.domain.BalanceSheetEntryType;
import com.alejochang.nethworthtrackerapi.domain.BalanceSheetTotals;
import com.alejochang.nethworthtrackerapi.utils.IMoneyCalculator;
import com.alejochang.nethworthtrackerapi.utils.MoneyCalculator;

@ExtendWith(MockitoExtension.class)
public class NetWorthServiceTests {

	private IMoneyCalculator moneyCalculator;
	
	@Mock
	private IExchangeService exchangeService;

	
	@InjectMocks
	private NetWorthService netWorthService;

	final Currency US = Currency.getInstance(Locale.US);
	
	@BeforeEach
	void setUp() {
		moneyCalculator = new MoneyCalculator();
		netWorthService = new NetWorthService(moneyCalculator, exchangeService);
	}

	@Test
	void savedUserHasRegistrationDate() {
		
		List<BalanceSheetEntry> assets = new ArrayList<BalanceSheetEntry>();
		List<BalanceSheetEntry> liabilities = new ArrayList<BalanceSheetEntry>();
		
		Supplier<Stream<BigDecimal>> assetAmountsSupplier 
		  = () -> Stream.of(BigDecimal.valueOf(1000), BigDecimal.valueOf(2000), BigDecimal.valueOf(3000));
		  
		  Supplier<Stream<BigDecimal>> liabilityAmountsSupplier 
		  = () -> Stream.of(BigDecimal.valueOf(6000), BigDecimal.valueOf(9000), BigDecimal.valueOf(12000));
		  
		Random random = new Random();

		BigDecimal totalAssets = assetAmountsSupplier.get().reduce(BigDecimal.ZERO, (acc, amount) -> amount.add(acc));
		BigDecimal totalLiabilities = liabilityAmountsSupplier.get().reduce(BigDecimal.ZERO, (acc, amount) -> amount.add(acc));
		BigDecimal netWorth = totalAssets.subtract(totalLiabilities);

		assets = assetAmountsSupplier.get()
				.map(amount -> 
				new BalanceSheetEntry("desc-asset-" + amount, amount, US,
					(random.nextBoolean() ? BalanceSheetEntryType.SHORT_TERM : BalanceSheetEntryType.LONG_TERM)))
				.collect(Collectors.toList());
		liabilities = liabilityAmountsSupplier.get()
				.map(amount -> 
				new BalanceSheetEntry("desc-liability-" + amount, amount, US,
					(random.nextBoolean() ? BalanceSheetEntryType.SHORT_TERM : BalanceSheetEntryType.LONG_TERM)))
				.collect(Collectors.toList());

		BalanceSheet balanceSheet = new BalanceSheet(US, assets, liabilities);

		BalanceSheetTotals totals =  netWorthService.calculateBalanceSheetTotals(balanceSheet);
		
		assertEquals(totals.getAssetsTotal(), totalAssets);
		assertEquals(totals.getLiabilitiesTotal(), totalLiabilities);
		assertEquals(totals.getNetWorthTotal(), netWorth);
	}

}
