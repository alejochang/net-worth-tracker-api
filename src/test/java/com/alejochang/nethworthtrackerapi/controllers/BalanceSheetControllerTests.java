package com.alejochang.nethworthtrackerapi.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.alejochang.nethworthtrackerapi.domain.BalanceSheet;
import com.alejochang.nethworthtrackerapi.domain.BalanceSheetTotals;
import com.alejochang.nethworthtrackerapi.services.INetWorthService;
import com.alejochang.nethworthtrackerapi.utils.IJsonResponseBuilder;

@ExtendWith(MockitoExtension.class)
public class BalanceSheetControllerTests {
	
	@InjectMocks
	BalanceSheetController balanceSheetController;
	
	@Mock
	INetWorthService netWorthService;
	
	@Mock
	IJsonResponseBuilder jsonResponseBuilder;
	
	
	@Test
	public void testNetWorthCalculation() {
		
        BalanceSheet balanceSheet = new BalanceSheet();
        
        BalanceSheetTotals expectedTotals = new BalanceSheetTotals(BigDecimal.valueOf(100000), BigDecimal.valueOf(10000), BigDecimal.valueOf(9000));
        
        String expectedResponse = "{"
        		+ "assetsTotal: 10000, "
        		+ "liabilitiesTotal:10000, "
        		+ "netWorthTotal: 9000"
        		+ "}";
        
        when(netWorthService.calculateBalanceSheetTotals(any(BalanceSheet.class))).thenReturn(expectedTotals);
        when(jsonResponseBuilder.buildJson(any(BalanceSheetTotals.class))).thenReturn(expectedResponse);
        
        ResponseEntity<String> responseEntity = balanceSheetController.calculateNethWorth(balanceSheet);
        
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseEntity.getBody()).isEqualTo(expectedResponse);
		
	}
	
	@Test
	public void testBalanceSheetConvertion() {
        
        BalanceSheet convertedBalanceSheet = new BalanceSheet();
        
        String expectedResponse = "{"
        		+ "assets: [], "
        		+ "liabilities:[], "
        		+ "currency: USD"
        		+ "}";
        
        when(netWorthService.applyFXToBalanceSheet(any(BalanceSheet.class), (any(Currency.class)))).thenReturn(convertedBalanceSheet);
        when(jsonResponseBuilder.buildJson(any(BalanceSheet.class))).thenReturn(expectedResponse);
        
        ResponseEntity<String> responseEntity = balanceSheetController.convertBalanceSheet(convertedBalanceSheet, Currency.getInstance(Locale.US).getCurrencyCode());
        
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseEntity.getBody()).isEqualTo(expectedResponse);
		
	}

}
